﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_4Diena1Uzdevums
{
    public class Professor : Person, IPerson
    {
        public Professor(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        public void GetHomework()
        {
            string fullName = GetFullName();
            Console.WriteLine($"Professor {fullName} has no homework!");
        }

        public override void GetHomework2()
        {

        }
        
        



    }
}
