﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_4Diena1Uzdevums
{
    public abstract class Person
    {
        public string Name;
        public string Surname;
        

        public string GetFullName()
        {
            return Name + " " + Surname;
        }

        public virtual void GetHomework2()
        {
            string fullName = GetFullName();
            Console.WriteLine($"Person {fullName} has homework!");
        }
        
           
        
         
   
     }
}
