﻿using System;
using System.Collections.Generic;

namespace RCS_4Diena1Uzdevums
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IPerson> persons = new List<IPerson>();

            persons.Add(new Student("Sandis", "Ozols"));
            persons.Add(new Professor("Juris", "Ābols"));


            foreach (IPerson person in persons)
            {
               person.GetHomework();
               person.GetHomework2();
            }
            Console.ReadKey();
        }
    }
}

