﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_4Diena1Uzdevums
{
    public class Student : Person,IPerson
    {
        public Student(string name,string surname)
        {
            Name = name;
            Surname = surname;
        }

        public void GetHomework()
        {
            string fullName = GetFullName();
            Console.WriteLine($"Student {fullName} has no homework!");
        }
    }
}
