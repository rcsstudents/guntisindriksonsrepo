﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace RCS_5Diena
{
    class Program
    {
        static void Main(string[] args)
        {
            ///*lai ievietotu jaunu tekstu
            // * using (StreamWriter writer = new StreamWriter("teksts.txt"))
            //{
            //    writer.WriteLine("Pirmais teksts faila!");
            //}*/

            ////lai pievienotu datus faila
            //using (StreamWriter writer = File.AppendText("teksts.txt"))
            //{
            //    writer.WriteLine("Jauns teksts!");
                
            //}
            //List<string> tekstaSaraksts = new List<string>();
            //using(StreamReader reader = new StreamReader("teksts.txt"))
            //{
            //    string line;
                
            //    while((line = reader.ReadLine()) != null)
            //    {
            //        tekstaSaraksts.Add(line);
            //    }
            //}

            //foreach(string tekstaRinda in tekstaSaraksts)
            //{
            //    Console.WriteLine(tekstaRinda);
            //}
            //File.WriteAllText("teksts.txt", String.Empty);

            QuizQuestion quizQuestion = new QuizQuestion();
            quizQuestion.Question = "Jautajums";
            quizQuestion.Answer = "Atbilde?";
            string json = JsonConvert.SerializeObject(quizQuestion);
            

            using (StreamWriter writer = new StreamWriter("MyJson.txt"))
            {
                writer.WriteLine(json);

            }
            string jsonFromFile;
            using (StreamReader reader = new StreamReader("MyJson.txt"))
            {
                 jsonFromFile = reader.ReadToEnd();

            }
            
                


                QuizQuestion deserialized = JsonConvert.DeserializeObject<QuizQuestion>(json);

            Console.WriteLine(deserialized.Question);
            Console.WriteLine(deserialized.Answer);

            

            Console.ReadKey();


        }
    }
}
