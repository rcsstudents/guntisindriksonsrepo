﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_5Diena
{
    [Serializable]
    class QuizQuestion
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
