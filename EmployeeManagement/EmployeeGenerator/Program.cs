<<<<<<< HEAD
﻿using DataManager.ManageData;
=======
﻿using DataManager;
using DataManager.ManageData;
>>>>>>> origin/UniversityManagement
using Models.EmployeeModels;
using System;

namespace EmployeeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Employee Generator");
            try
            {
                GenerateEmployees generateEmployee = new GenerateEmployees();
                Employee employee = generateEmployee.GenerateEmployee();

                DataWriter writer = new DataWriter(DataConfiguration.EmployeeData);
                writer.WriteEmployee(employee);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
