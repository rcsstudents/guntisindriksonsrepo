﻿using DataManager;
using DataManager.ManageData;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityManagement.Managers
{
    class StudentManager : IManager
    {
        List<Student> Students;
        DataReader reader;
        DataWriter writer;

        public StudentManager()
        {
            reader = new DataReader(DataConfiguration.StudentData);
            writer = new DataWriter(DataConfiguration.StudentData);
            Students = reader.ReadData<Student>();
        }
        public void Create()
        {
            

            Student student = new Student();
            Console.WriteLine("Enter student ID:");
            student.Id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter student name:");
            student.Name = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Enter student Surname:");
            student.Surname = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Enter student UniversityID:");
            student.UniversityId = Convert.ToInt32(Console.ReadLine());

            Students.Add(student);
            writer.WriteData(Students);
            
            
        }

        public void Delete()
        {
            

            int id = 1;
            Students = Students.Where(u => u.Id != id).ToList();
            writer.WriteData(Students);
        }

        public void Read()
        {
            

            foreach (Student student in Students)
            {
                Console.WriteLine($"{student.Id} {student.Name}");
            }

        }

            public void Update()
        {
            


            int id = 1;
            Student student = Students.FirstOrDefault(u => u.Id == id);
            if (student == null)
            {
                Console.WriteLine($"Students ar Id={1} neeksiste");
                return;
            }

            student.Name = "Guntis";

            writer.WriteData(Students);
        }
    }
}
