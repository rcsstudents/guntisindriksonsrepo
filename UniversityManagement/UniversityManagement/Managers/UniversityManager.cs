﻿using DataManager;
using DataManager.ManageData;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityManagement.Managers
{
    class UniversityManager : IManager
    {
        List<University> Universities;
        DataReader reader;
        DataWriter writer;

        public UniversityManager()
        {
            reader = new DataReader(DataConfiguration.UniversityData);
            writer = new DataWriter(DataConfiguration.UniversityData);
            Universities = reader.ReadData<University>();

        }
        public void Create()
        {
            
            University university = new University();
            university.Id = 1;
            university.Name = "LU";

            Universities.Add(university);
            writer.WriteData(Universities);
        }

        public void Delete()
        {
            

            int id = 1;
            Universities = Universities.Where(u => u.Id != id).ToList();
            writer.WriteData(Universities);
        }

        public void Read()
        {
            

            foreach(University university in Universities)
            {
                Console.WriteLine($"{university.Id} {university.Name}");
            }
        }

        public void Update()
        {
            

            int id = 1;
            University university = Universities.FirstOrDefault(u => u.Id == id);
            if(university == null)
            {
                Console.WriteLine($"Universitate ar Id={1} neeksiste");
                return;
            }

            university.Name = "RTU";

            writer.WriteData(Universities);
        }
    }
}
